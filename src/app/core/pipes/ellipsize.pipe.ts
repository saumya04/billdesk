import { Pipe, PipeTransform } from '@angular/core';

@Pipe({name: 'ellipsize'})
export class EllipsizePipe implements PipeTransform {
  
  transform(value: string, max: any, ellipse: any, truncate: any): any {
    if (!value) return value;

    let options = {
      ellipse: (ellipse) ? ellipse : '…',
      max: (max) ? max : 140,
      chars: [' ', '-'],
      truncate: (truncate) ? ((truncate == '1') ? true : false) : true
  	};

  	return this.ellipsize(value, options.max, options.ellipse, options.chars, options.truncate);
  }

  ellipsize(str = '', max = 140, ellipse, chars, truncate) {
    let last = 0;
    let c = '';

    if (str.length < max) return str;

    for (let i = 0, len = str.length; i < len; i++) {
      c = str.charAt(i);

      if (chars.indexOf(c) !== -1) {
          last = i;
      }

      if (i < max) continue;
      if (last === 0) {
          return !truncate ? '' : str.substring(0, max - 1) + ellipse;
      }

      return str.substring(0, last) + ellipse;
    }

    return str;
	}
}