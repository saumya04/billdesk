import { Pipe, PipeTransform } from '@angular/core';

@Pipe({name: 'limit_str'})
export class LimitStrPipe implements PipeTransform {
  transform(value: string, args: any): any {
    if (!value) return value;

        return value.substring(0, args);
  }
}