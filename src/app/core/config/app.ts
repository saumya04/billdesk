import { Injectable } from '@angular/core';
import { Title, Meta }  from '@angular/platform-browser';
import { ApiRoutes } from './api-routes';
import { environment } from '../../../environments/environment';
import * as _ from 'lodash';

@Injectable()
export class App {

  constructor(
  		private titleService: Title,
      private apiRoutes: ApiRoutes,
      private metaService: Meta,
	) { }

  public setTitle(newTitle: string) {
    this.titleService.setTitle(newTitle);
  }

  get getApiRoutes() {
  	return this.apiRoutes;
  }

}
