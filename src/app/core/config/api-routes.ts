import { Injectable } from '@angular/core';
import { environment } from '../../../environments/environment';

@Injectable()
export class ApiRoutes {

  private apiSecretKeyName = environment.server.apiSecretKeyName;
  private apiSecretKey = environment.server.apiSecret;
  // private apiVersionUrl = environment.server.apiVersionPath;
  // private baseUrl = environment.server.baseUrl;
  // private apiTokenKeyName = environment.server.apiTokenKeyName;
  // private imageHostingBaseUrl = environment.server.imageHostingBaseUrl;

  constructor() { }

  get getApiSecretKeyName(): string {
    return this.apiSecretKeyName;
  }

  // get getApiTokenKeyName(): string {
  //   return this.apiTokenKeyName;
  // }

  get getApiSecretKey(): string {
    return this.apiSecretKey;
  }

  // get getBaseUrl(): string {
  //   return this.baseUrl + this.apiVersionUrl;
  // }

  // get getImageHostingBaseUrl(): string {
  //   return this.imageHostingBaseUrl;
  // }

  // ================= API URLs Here =================
  
}
