import { Injectable, OpaqueToken } from '@angular/core';

@Injectable()
export class Storage {

	private isBrowser = (typeof localStorage !== 'undefined');
	private storage: any = (this.isBrowser) ? localStorage : {};

	get(key) {
    if(this.isBrowser) {
      let item = this.storage.getItem(key);

      try {
        return JSON.parse(item);
      } catch (err) {
        return item;
      }
    }
	}

	set(key, value) {
    if(this.isBrowser) {
      if(typeof value === "object") {
        this.storage.setItem(key, JSON.stringify(value));
      } else {
        this.storage.setItem(key, value);
      }
    }
  }

  remove(key) {
    if(this.isBrowser) {
      this.storage.removeItem(key);
    }
  }

  clear() {
    if(this.isBrowser) {
      this.storage.clear();
    }
  }

}
