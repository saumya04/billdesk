import { Injectable, ReflectiveInjector } from '@angular/core';
import * as _ from 'lodash';

@Injectable()
export class Helper {

  static isEmpty(value) {
    let bool = value == null ||
              (typeof value === 'string' && value.length === 0) ||
              (Helper.isArray(value) && value.length === 0);

    if(typeof value === 'object') {
      for (let key in value) {
          if(value.hasOwnProperty(key))
              return false;
      }
      return true;
    }

    return bool;
  }

  static getErrorMessage(error) {
    if(Helper.hasValidationErrors(error)) {
      return error.json();
    }
    return Helper
  }

  static getValidationErrorObj(error) {
    let errorObj: any;
    if(Object.keys(error).length !== 0 && error.constructor === Object) {
      errorObj = error
    } else {
      errorObj = error.json();
    }
    let error_messages = [];
    if(errorObj.hasOwnProperty('errors')) {
      let errorMessagesObj = errorObj.errors;
      for(let key in errorMessagesObj) {
        error_messages.push(errorMessagesObj[key])
      }
    }
    return (error_messages.length > 0) ? error_messages : ['Something went wrong, try again later!'];
  }

  static getErrorObject(error) {
    return error.json();
  }

  static hasValidationErrors(error): boolean {
    if(error.hasOwnProperty('status') || error.hasOwnProperty('code')) {
      return (error.hasOwnProperty('status')) ?
        ((error.status == 422) ? true : false) : ((error.code == 422) ? true : false);
    }
    return false
  }

  static getQueryParamsString(paramsObj: Object = {}): string {
    let parts = [];
    for (var i in paramsObj) {
      if (paramsObj.hasOwnProperty(i)) {
        parts.push(encodeURIComponent(i) + "=" + encodeURIComponent(paramsObj[i]));
      }
    }
    return '?' + parts.join("&");
  }

  static getTransformedErrorResponse(json_response: any) {
    // In case of ErrorObservable
    console.log('----> ', json_response);
    if(json_response.constructor.name == "ErrorObservable" || json_response.constructor.name === "e") {
      if(json_response.hasOwnProperty('_body') && this.isString(json_response._body)) {
        json_response = JSON.parse(json_response._body);
      }
    }

    if(! json_response) {
      return null;
    }

    if(Helper.hasValidationErrors(json_response)) {
      return Helper.getValidationErrorObj(json_response);
    } else if(typeof json_response === 'object' && json_response.hasOwnProperty('message')) {
      return [json_response.message];
    } else if(typeof json_response === "string") {
      return [JSON.parse(json_response).message];
    }
    return null;
  }

  static processErrorResponse(error: any, toastr: any) {
    let error_messages = Helper.getTransformedErrorResponse(error);
    if(error_messages && error_messages.length > 0) {
      toastr.showError(error_messages[0]);
    } else {
      toastr.showError();
    }
  }

  static getTransformedUrlPathFromActivatedRoute(route: any) {
    let pathroots = route.pathFromRoot;
    let pathUrlFromRoot = '';
    pathroots.forEach(path => {
        path.url.subscribe(url => {
             url.forEach(e => {
                pathUrlFromRoot += e + '/';
             });
        });
    });
    return pathUrlFromRoot;
  }

  static getPreviousUrlString(url_string: string, segment: number = 1) {
    let arr = url_string.split('/');
    arr = arr.filter(function(str) {
    	return (str != '');
    });
    for(let i = 0; i < segment; i++) {
      arr.splice(-1, 1);
    }
    return '/' + arr.join('/');
  }

  static getPreviousUrlStringFromActivatedRoute(route: any, segment: number = 1) {
    let url_str = Helper.getTransformedUrlPathFromActivatedRoute(route);
    return Helper.getPreviousUrlString(url_str, segment);
  }

  static isInt(n){
    return Number(n) === n && n % 1 === 0;
  }

  static isFloat(n){
    return Number(n) === n && n % 1 !== 0;
  }

  static isNumeric(n) {
    return !isNaN(parseFloat(n)) && isFinite(n);
  }

  static isArray(value) {
    return Object.prototype.toString.call(value) === '[object Array]';
  }

  static isString(str) {
    return typeof str === 'string';
  }

  // Element Offet
  static elementOffset(elt) {
    let rect = elt.getBoundingClientRect(), bodyElt = document.body;
    return {
      top: rect.top + bodyElt .scrollTop,
      left: rect.left + bodyElt .scrollLeft
    }
  }

  static cloneObj(obj: Object, extraData: Object = {}) {
    return Object.assign(obj, extraData);
  }

  static formatNumber(number, decimals = 0, dec_point = null, thousands_sep = null) {
    let n = !isFinite(+number) ? 0 : +number, 
        prec = !isFinite(+decimals) ? 0 : Math.abs(decimals),
        sep = (thousands_sep === null) ? ',' : thousands_sep,
        dec = (dec_point === null) ? '.' : dec_point,
        toFixedFix = function (n, prec) {
            // Fix for IE parseFloat(0.55).toFixed(0) = 0;
            let k = Math.pow(10, prec);
            return Math.round(n * k) / k;
        },
        s = (prec ? toFixedFix(n, prec) : Math.round(n)).toString().split('.');
    if (s[0].length > 3) {
        s[0] = s[0].replace(/\B(?=(?:\d{3})+(?!\d))/g, sep);
    }
    if ((s[1] || '').length < prec) {
        s[1] = s[1] || '';
        s[1] += new Array(prec - s[1].length + 1).join('0');
    }
    return s.join(dec);
  }

  static convertRuppesTo(value: any, type: string = '') {
    let returnVal = null;
      
    switch(type) {
      case 'paise':
        returnVal = value * 100;
        break;
      case 'rupees':
        returnVal = value / 100;
        break;
      default:
        returnVal = value;
        break;
    }

    return returnVal;
  }

  static nl2br(str: string, isXhtml: boolean = false){
      if (typeof str === 'undefined' || str === null) {
        return ''
      }
      // Adjust comment to avoid issue on locutus.io display
      var breakTag = (isXhtml || typeof isXhtml === 'undefined') ? '<br ' + '/>' : '<br>'
      return (str + '')
        .replace(/(\r\n|\n\r|\r|\n)/g, breakTag + '$1')
  }

  static getObjProp(dotNotationStr: string, obj: Object, defaultVal: any = null) {
    let value = _.property(dotNotationStr)(obj);
    return (this.isEmpty(value)) ? defaultVal : value;
  }

  static isMobileDevice() {
    var isMobile = false; //initiate as false
    // device detection
    if(/(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|ipad|iris|kindle|Android|Silk|lge |maemo|midp|mmp|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows (ce|phone)|xda|xiino/i.test(navigator.userAgent) 
        || /1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i.test(navigator.userAgent.substr(0,4))) isMobile = true;
    return isMobile;
  };

  static getUserGeoLocation(successCb, failureCb = null) {
    failureCb = failureCb || function(){};
    
    if (navigator.geolocation) {
      var options = {
        enableHighAccuracy: true,
        timeout: 5000,
        maximumAge: 0
      };
      
      let success = function(pos) {
        var crd = pos.coords;
        console.log('Your current position is:');
        console.log(`Latitude : ${crd.latitude}`);
        console.log(`Longitude: ${crd.longitude}`);
        console.log(`More or less ${crd.accuracy} meters.`);
        successCb(pos);
      };

      let error = function(err) {
        console.warn(`ERROR(${err.code}): ${err.message}`);
        if(failureCb) {
          failureCb()
        }
      };

      return navigator.geolocation.getCurrentPosition(success, error);
    } else {
      console.log("No location service detected!");
      if(failureCb) {
        failureCb();
      }
    }
  }

  static propExists(obj: Object = {}, dotNotationStr: string = '') {
    let result = _.property(dotNotationStr)(obj);
    return (result) ? true : false;
  }

}
