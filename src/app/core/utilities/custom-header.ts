import { Injectable } from '@angular/core';
import { Headers, RequestOptions } from '@angular/http';
import { App as AppConfig } from '../config/app';
import { Storage } from './storage';
import { Helper } from './helper';

@Injectable()
export class CustomHeader {
  headers: Headers;
  private storageAuthObj: any = {};

  constructor(public appConfig: AppConfig, private storage: Storage) {}

  get getCommonHeaders() {
    this.headers = new Headers();
    this.headers.append('Accept', 'application/json');
    this.headers.append('Content-Type', 'application/json');
    return this.headers;
  }

  secure() {
    let content = this.getCommonHeaders;
    return content;
  }

  secureWithToken() {
    return this.getCommonHeaders;
    // this.storageAuthObj = (this.storage.get('auth_user')) ? this.storage.get('auth_user') : {};
    // let token = Helper.getObjProp('token.value', this.storageAuthObj);
    // content.append('api-secret', this.appConfig.getApiRoutes.getApiSecretKey);
    // content.append('api-token', token);
    // return content;
  }

  securedMultipartRequest() {
    this.headers = new Headers();
    this.headers.append('Accept', '*/*');
    this.headers.append('Content-Type', 'multipart/form-data');
    this.headers.append('api-secret', this.appConfig.getApiRoutes.getApiSecretKey);
    this.headers.append('api-token', this.storage.get('token'));
    return this.headers;
    // content.append('enctype', 'multipart/form-data');
    // return content;
  }

  toRequestOptions(content) {
    return new RequestOptions({ headers: content });
  }
}
