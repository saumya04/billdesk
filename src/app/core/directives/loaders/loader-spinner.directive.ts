import { Component, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'loader-spinner',
  template: `
    <div *ngIf="show()"
        class="loader-full-circle-small"
        [ngClass]="classObj"></div>
  `
})
export class LoaderSpinner {

  @Input() showLoader: boolean = false;
  @Input() classObj: any = {};

  show() {
    return this.showLoader;
  }

  constructor() { }
}
