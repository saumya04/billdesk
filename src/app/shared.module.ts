import { NgModule } 							              from '@angular/core';
import { CommonModule }                     	  from '@angular/common';
import { Title, Meta, BrowserModule }           from "@angular/platform-browser";
import { BrowserAnimationsModule }              from '@angular/platform-browser/animations';
import { ControlMessages } 					            from './core/utilities/validators/control-messages';
import { App as AppConfig } 					          from './core/config/app';
import { ApiRoutes }                            from './core/config/api-routes';
import { Storage } 								              from './core/utilities/storage';
import { Http, Response, HttpModule }           from '@angular/http';
import { CustomHeader }                         from './core/utilities/custom-header';
import { Helper }                               from './core/utilities/helper';

import { TrimStrPipe }                          from './core/pipes/trim-str.pipe';
import { LimitStrPipe }                         from './core/pipes/limit-str.pipe';
import { EllipsizePipe }                        from './core/pipes/ellipsize.pipe';
import { SafePipe }                             from './core/pipes/safe.pipe';

import { FormsModule, ReactiveFormsModule }     from '@angular/forms';

// Directives
import { LoaderSpinner }                        from './core/directives/loaders/loader-spinner.directive';
import { ToastrService }                        from 'ngx-toastr';
import { Toastr } 								              from './core/plugins/toastr/toastr';

@NgModule({
    imports: [
      CommonModule,
      HttpModule,
      FormsModule,
      ReactiveFormsModule,
    ],
    declarations: [
      ControlMessages,
      LoaderSpinner,
      LimitStrPipe,
      TrimStrPipe,
      EllipsizePipe,
      SafePipe,
    ],
    exports: [
    	CommonModule,
      ControlMessages,
      HttpModule,
      LoaderSpinner,
      LimitStrPipe,
      TrimStrPipe,
      EllipsizePipe,
      SafePipe,
      FormsModule,
      ReactiveFormsModule,
  	],
  	providers: [
    	Toastr,
      ApiRoutes,
      AppConfig,
      Title,
      Meta,
      Storage,
      CustomHeader,
      Helper,
      ToastrService,
    ]
})
export class SharedModule {}
